from django.contrib import admin
from projects.models import Project
from tasks.models import Task


class ReceiptAdmin(admin.ModelAdmin):
    pass


admin.site.register(Project)
admin.site.register(Task)
