from django.urls import path
from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
)
from django.views.generic.base import RedirectView

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("", RedirectView.as_view(), name="home"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
